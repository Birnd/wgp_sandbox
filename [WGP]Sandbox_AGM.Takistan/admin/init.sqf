/* ---------------------------- */
/* Nur Client oder ServerClient */
/* ---------------------------- */


waitUntil {!isNull player};
waitUntil {!isNull ZeusModule1};
waitUntil {alive player};
sleep 5; // Ensures all curator modules are loaded . I don't like it any more than you do.

if (player call wgp_fnc_isZeus) then {
    systemchat "INFO: You are godlike!";
    pVAR_changeRank = [player, "COLONEL"]; publicVariable "pVAR_changeRank";
    player execVM "admin\initonrespawn.sqf";
    player addEventHandler ["killed", {_this execVM "admin\initonrespawn.sqf"}];
    execVM "admin\ai_skill_mod.sqf";
    #include "KKSB\KKSB.sqf"
    (getAssignedCuratorLogic player) call bis_fnc_drawCuratorLocations; // Markers for cities
    
};