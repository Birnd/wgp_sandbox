// Hide script markers
{
	_x setmarkerAlphaLocal 0;
} foreach ["m_armory", "m_base", "m_base_1", "m_base_2", "m_base_3", "respawn_west","respawn_east" ,"respawn_guerrila", "respawn_civilian" ];
rep_triggers = [[repland_base, "respawn_armor"],[repheli_base,"respawn_air"],[repjet_base,"respawn_plane"]]; // Repair base triggers
{
	_trigger = _x select 0;
    _name = _x select 1;
	
	_color = "ColorBlue";
	
	_mflag = createmarkerLocal [_name, getpos _trigger];
	_mflag setmarkershapeLocal "ICON";
	_mflag setmarkertypeLocal _name;
	//_mflag setmarkertextLocal _name;
    //_mflag setmarkersizeLocal [0.1,0.1];
	_mflag setMarkerColorLocal _color;
	//_mflag setMarkerAlpha 0.5;
    
	_mborder = createmarkerLocal [_name + "_border",getpos _trigger];
	if ((triggerarea _trigger) select 3)
		then {_mborder setmarkershapeLocal "RECTANGLE" }
		else { _mborder setmarkershapeLocal "ELLIPSE"};
	_mborder setmarkersizeLocal [(triggerArea _trigger) select 0, (triggerArea _trigger) select 1];
	_mborder setMarkerDirLocal ((triggerArea _trigger) select 2) select 0;
	_mborder setMarkerColorLocal _color;
	_mborder setMarkerBrushLocal "Solid";
    _mborder setMarkerAlphaLocal 0.5;
	_trigger setVariable ["Marker1", _mflag, true];
	_trigger setVariable ["Marker2", _mborder, true];
} forEach rep_triggers;

waituntil {!isnil "bis_fnc_init"};
[] spawn compile preprocessFileLineNumbers "initMarkers.sqf";

tf_same_sw_frequencies_for_side = true;
tf_same_lr_frequencies_for_side = false;
TF_give_microdagr_to_soldier = false;

setviewdistance 3500;
setObjectViewDistance 3500;
setTerrainGrid 3.125;

if (!isServer) then { waitUntil {!isNull player};};
enableSaving [false, false];

// TFAR settings
TF_terrain_interception_coefficient = (paramsArray select 0);
tf_same_sw_frequencies_for_side = true;
tf_same_lr_frequencies_for_side = true;
TF_give_microdagr_to_soldier = false;

// AGM Settings
AGM_Interaction_EnableTeamManagement = false;


cutText ["Init...", "BLACK FADED",1];


// PUBLIC Variable Eventlistener
"pVAR_changeGroupID" addPublicVariableEventHandler { (_this select 1) spawn wgp_fnc_setGroupID }; //Gruppenname
"pVAR_changeRank" addPublicVariableEventHandler { (_this select 1) spawn wgp_fnc_setRank }; // Spielerrang


if (!isServer) then {
    //Server über Connect informieren
	pV_RequestClientId = player;
	publicVariableServer "PV_RequestClientId";
    // diag_log("DEBUG: JIP-ID an Server gesendet.");
};
if (hasInterface) then {
	player setVariable ["playerKitID", -1]; // player has to remember what kit he has. E.g. for kit customization.
	player addEventHandler ["Respawn", {
        if (player getVariable "playerKitID" != -1) then {
	    	_scriptFilename = loadouts select (player getVariable "playerKitID") select 1;
			systemchat ("INFO: Respawn as: " + (loadouts select (player getVariable "playerKitID") select 0));
			this = player;
            try {
				call compile preprocessFileLineNumbers ("barracks\loadouts\" + str (side player) + "\" + _scriptFilename + ".sqf");
            } catch {
                    // Start Loadout
					removeAllWeapons player;
					removeAllItems player;
					removeAllAssignedItems player;
					removeUniform player;
					removeVest player;
					removeBackpack player;
					removeHeadgear player;
					removeGoggles player;
				    if (side player == West) then { player forceAddUniform "rhs_uniform_cu_ocp_patchless"; };
				    if (side player == East) then { player forceAddUniform "rhs_uniform_mflora_patchless"; };
				    _resistance_uniforms = ["U_CAF_AG_ME_ROBES_mil_01a",
				    "U_CAF_AG_ME_ROBES_01", "U_CAF_AG_ME_ROBES_01a", "U_CAF_AG_ME_ROBES_01b", "U_CAF_AG_ME_ROBES_01c", "U_CAF_AG_ME_ROBES_01d",
				    "U_CAF_AG_ME_ROBES_02", "U_CAF_AG_ME_ROBES_02a", "U_CAF_AG_ME_ROBES_02b", "U_CAF_AG_ME_ROBES_02c",
				    "U_CAF_AG_ME_ROBES_03", "U_CAF_AG_ME_ROBES_03a", "U_CAF_AG_ME_ROBES_03b", "U_CAF_AG_ME_ROBES_03c",
				    "U_CAF_AG_ME_ROBES_04", "U_CAF_AG_ME_ROBES_04a", "U_CAF_AG_ME_ROBES_04b", "U_CAF_AG_ME_ROBES_04c"
				    ];
					_random = _resistance_uniforms select floor random count _resistance_uniforms;
				    if (side player == Resistance) then { player forceAddUniform _random; };
				    if (side player == Civilian) then { player forceAddUniform "U_C_Journalist"; };
					player addItemToUniform "AGM_EarBuds";
					player linkItem "ItemMap";
					player linkItem "ItemCompass";
					player linkItem "ItemWatch";
					player linkItem "ItemRadio";
            };
            this = Nil;
        };
    }];
    
    // Start Loadout
	removeAllWeapons player;
	removeAllItems player;
	removeAllAssignedItems player;
	removeUniform player;
	removeVest player;
	removeBackpack player;
	removeHeadgear player;
	removeGoggles player;
    if (side player == West) then { player forceAddUniform "rhs_uniform_cu_ocp_patchless"; };
    if (side player == East) then { player forceAddUniform "rhs_uniform_mflora_patchless"; };
    _resistance_uniforms = ["U_CAF_AG_ME_ROBES_mil_01a",
    "U_CAF_AG_ME_ROBES_01", "U_CAF_AG_ME_ROBES_01a", "U_CAF_AG_ME_ROBES_01b", "U_CAF_AG_ME_ROBES_01c", "U_CAF_AG_ME_ROBES_01d",
    "U_CAF_AG_ME_ROBES_02", "U_CAF_AG_ME_ROBES_02a", "U_CAF_AG_ME_ROBES_02b", "U_CAF_AG_ME_ROBES_02c",
    "U_CAF_AG_ME_ROBES_03", "U_CAF_AG_ME_ROBES_03a", "U_CAF_AG_ME_ROBES_03b", "U_CAF_AG_ME_ROBES_03c",
    "U_CAF_AG_ME_ROBES_04", "U_CAF_AG_ME_ROBES_04a", "U_CAF_AG_ME_ROBES_04b", "U_CAF_AG_ME_ROBES_04c"
    ];
	_random = _resistance_uniforms select floor random count _resistance_uniforms;
    if (side player == Resistance) then { player forceAddUniform _random; };
    if (side player == Civilian) then { player forceAddUniform "U_C_Journalist"; };
	player addItemToUniform "AGM_EarBuds";
	player linkItem "ItemMap";
	player linkItem "ItemCompass";
	player linkItem "ItemWatch";
	player linkItem "ItemRadio";
};

if (isServer) then {
    //Spieler joint: Wichtige local Informationen übermitteln
	"PV_RequestClientId" addPublicVariableEventHandler {
        //diag_log("DEBUG: JIP-ID erhalten");
        
       _JIP_netid = owner (_this select 1);
       
       [_JIP_netid] spawn wgp_fnc_onPlayerConnected;  
	};
    "PV_Invisible" addPublicVariableEventHandler {
        // 0 = ClientPlayer; 1 = True/False (Invis/Vis)
        private["_player","_state"];
        _player = (_this select 1) select 0;
        _state = (_this select 1) select 1;
        _player hideObjectGlobal _state;
    };
};

/* Warten bis das Briefing beendet wurde */
sleep .1;

player setvariable ["BIS_nocoreconversations",true];
MY_SelfInteraction_ID = ["Squad management", {true}, {execVM "dialogGroupmanager\showModal.sqf";}, true] call AGM_Interaction_fnc_addInteractionSelf;

// Side Relations
Resistance setFriend [East, 0];
East setFriend [Resistance, 0];
Resistance setFriend [West, 0];
West setFriend [Resistance, 0];

/* Module initialisieren */
[] spawn compile preprocessFileLineNumbers "tfr\init.sqf"; // Task Force Radio Workarounds
[] spawn compile preprocessFileLineNumbers "admin\init.sqf"; // Admin Tools (O-Taste)
[] spawn compile preprocessFileLineNumbers "pixGps\init.sqf"; // Gruppenkartenmarker
[] spawn compile preprocessFileLineNumbers "credits\init.sqf"; // Credits
// [] spawn compile preprocessFileLineNumbers "xmedsys\init.sqf";
// execVM "IgiLoad\IgiLoadInit.sqf";
// execVM "BDD\Greifer.sqf";
// execVM "R3F_LOG\init.sqf";
[] spawn compile preprocessFileLineNumbers "init_fireStop.sqf"; // No-Shoot-Zone in Basis
if (isServer) then { ["m_base", "m_base_1", "m_base_2", "m_base_3"] spawn wgp_fnc_cleanBase; };


/* Camera deaktivieren */
cutText ["", "BLACK IN",1];