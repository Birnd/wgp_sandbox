private["_group","_newname"];

_group = _this select 0;
_newname = _this select 1;
// diag_log format["DEBUG: GroupID change: " + _newname];
_group setGroupId [format["%1",_newname] ];
_group setVariable ["CustomGroup", _newname];

true