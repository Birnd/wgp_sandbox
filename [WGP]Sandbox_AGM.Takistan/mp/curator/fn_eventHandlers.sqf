/* Inspired by Fett_Li
 * http://forums.bistudio.com/showthread.php?176691-Making-placed-units-be-editable-for-every-Zeus
 */


    {
    _x addEventHandler ["CuratorGroupPlaced",{[_this,"wgp_fnc_grpPlaced",false] spawn BIS_fnc_MP}];
	_x addEventHandler ["CuratorObjectPlaced",{[_this,"wgp_fnc_objPlaced",false] spawn BIS_fnc_MP}];
	} forEach allCurators;