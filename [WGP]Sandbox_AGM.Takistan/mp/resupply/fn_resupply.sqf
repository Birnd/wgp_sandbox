scopename "main";
_vehicle = _this;
if (local _vehicle) then {
	_type = typeOf _vehicle;
	_cfg = (configFile >> "CfgVehicles" >> _type);
	_displayname = if (isText(_cfg >> "displayName")) then { getText(_cfg >>"displayName")} else { _type };
	
	x_reload_time_factor = 3;
	
	_vehicle setVehicleAmmo 1;
	
	_vehicle vehicleChat format ["Performing maintenante on %1, please wait.", _displayname];
	
	_magazines = getArray(configFile >> "CfgVehicles" >> _type >> "magazines");
	
	if (count _magazines > 0) then {
		_removed = [];
		{
			if (!(_x in _removed)) then {
				_vehicle removeMagazines _x;
				_removed = _removed + [_x];
			};
		} forEach _magazines;
		{
	        if (isEngineOn _vehicle) exitwith {_vehicle vehicleChat format["Reload has been ABORTED! Don't fiddle with the engine!"]};
			_mname = if (isText(configFile >> "CfgMagazines" >> _x >> "displayName")) then { getText(configFile >> "CfgMagazines" >> _x >> "displayName")} else { _x };
			_vehicle vehicleChat format ["Loading %1", _mname];
			sleep x_reload_time_factor;
			_vehicle addMagazine _x;
		} forEach _magazines;
	};
	_count = count (configFile >> "CfgVehicles" >> _type >> "Turrets");
	if (_count > 0) then {
		for "_i" from 0 to (_count - 1) do {
			if (isEngineOn _vehicle) exitwith {_vehicle vehicleChat format["Reload has been ABORTED! Don't fiddle with the engine!"]};
			scopeName "xx_reload2_xx";
			_config = (configFile >> "CfgVehicles" >> _type >> "Turrets") select _i;
			_magazines = getArray(_config >> "magazines");
			_removed = [];
			{
				if (!(_x in _removed)) then {
					_vehicle removeMagazines _x;
					_removed = _removed + [_x];
				};
			} forEach _magazines;
			{
	            if (isEngineOn _vehicle) exitwith {_vehicle vehicleChat format["Reload has been ABORTED! Don't fiddle with the engine!"]};
	            _mname = if (isText(configFile >> "CfgMagazines" >> _x >> "displayName")) then { getText(configFile >> "CfgMagazines" >> _x >> "displayName")} else { _x };
				_vehicle vehicleChat format ["Loading %1", _mname];
				sleep x_reload_time_factor;
				_vehicle addMagazine _x;
				sleep x_reload_time_factor;
			} forEach _magazines;
			_count_other = count (_config >> "Turrets");
			if (_count_other > 0) then {
				for "_i" from 0 to (_count_other - 1) do {
					_config2 = (_config >> "Turrets") select _i;
					_magazines = getArray(_config2 >> "magazines");
					_removed = [];
					{
						if (!(_x in _removed)) then {
							_vehicle removeMagazines _x;
							_removed = _removed + [_x];
						};
					} forEach _magazines;
					{	
	                	if (isEngineOn _vehicle) exitwith {_vehicle vehicleChat format["Reload has been ABORTED! Don't fiddle with the engine!"]};
	                	_mname = if (isText(configFile >> "CfgMagazines" >> _x >> "displayName")) then { getText(configFile >> "CfgMagazines" >> _x >> "displayName")} else { _x };
						_vehicle vehicleChat format ["Loading %1", _mname]; 
						sleep x_reload_time_factor;
						_vehicle addMagazine _x;
						sleep x_reload_time_factor;
					} forEach _magazines;
				};
			};
		};
	};
	_vehicle setVehicleAmmo 1;	// Reload turrets / drivers magazine
	
	_vehicle vehicleChat "Repairing...";
	while { damage _vehicle > 0 } do {
	    if (isEngineOn _vehicle) exitwith {_vehicle vehicleChat format["Repairs have been ABORTED! Don't fiddle with the engine!"]};
	    _vehicle setDamage ((round(damage _vehicle * (10 ^ 2)) / (10 ^ 2)  - 0.01));
	    hint parseText format["<t color='#ffc600'><img image='\A3\ui_f\data\gui\rsc\rscdisplayarcademap\icon_debug_ca.paa'/> Repair status: %1&#37;</t>", 100* (1 - round(damage _vehicle * (10 ^ 2)) / (10 ^ 2))];
	    sleep (x_reload_time_factor / 3);
	    
	};
	
	
	_vehicle vehicleChat "Refueling...";
	while {fuel _vehicle < 0.99} do {
	    
	    if (isEngineOn _vehicle) exitwith {_vehicle vehicleChat format["Refueling has been ABORTED! Don't fiddle with the engine!"]};
		_vehicle setFuel ((fuel _vehicle + 0.01));
		//_vehicle setFuel 1;
		sleep (x_reload_time_factor / 3);
	};
	sleep x_reload_time_factor;
	_vehicle vehicleChat format ["%1 is good as new!", _displayname];
};