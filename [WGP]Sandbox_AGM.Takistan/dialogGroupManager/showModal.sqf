/* Dialog erstellen*/
#include "defines.hpp";
createDialog "PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER"; 	

/* Gruppen zusammensuchen */
private["_groups"];
_groups = [];
{
	if (!(group _x in _groups) && side group _x == side player) then {
		private["_group","_CustomGroup","_gname"];
		_group = group _x;
		_CustomGroup = _group getVariable "CustomGroup";
		if (!isNil "_CustomGroup") then
		{
			_groups = _groups + [_group];
		};
		_gname = groupID _group;
	};
} foreach playableUnits;

/* Listbox füllen*/
{
	lbAdd [PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_List, groupID _x];
    _squadinfo = squadParams leader _x select 0;
    if (!isNil "_squadinfo") then {
    	if (count _squadinfo > 0) then {
            lbSetPicture[PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_List, (lbSize PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_List) -1, _squadinfo select 4];
            lbSetPictureColor[PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_List, (lbSize PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_List) -1, [1,1,1,1]];
        };
    };
} foreach _groups;
_squadinfo = squadParams player select 0;
if (!isNil "_squadinfo") then {
    if (count _squadinfo > 0) then { ctrlSetText [PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_EditName, _squadinfo select 0]};
};
if (count _groups > 0) then {
	lbSetCurSel [PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_List, 0];
};

/*-----------------------------------------------------------------	*/
/* Dialog anzeigen*/
pixLogisticDialogHqGroupManager_Button = -1;
pixLogisticDialogHqGroupManager_Selection = -1;
waitUntil { !dialog };
/*-----------------------------------------------------------------	*/

// Selected
if ((pixLogisticDialogHqGroupManager_Button == 0) && (pixLogisticDialogHqGroupManager_Selection != -1)) then 
{
    private "_ctrl_members";
	_ctrl_members = PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_Members_List;
    _group = _groups select pixLogisticDialogHqGroupManager_Selection;
    // player sidechat name _group;
    {
    	lbAdd [_ctrl_members, name _x];
    } foreach units _group;
};




/* Create */
if (pixLogisticDialogHqGroupManager_Button == 1) then {
    _isnew = true;
    {
        if (gnewname == groupID _x) exitwith {
            systemchat ("INFO: Sorry, a group with this name already exists!");
            _isnew = false;
        } ;
        
    } foreach _groups;
    if (_isnew) then {
		player setVariable ["CustomGroup",true,true];
	    pVAR_changeRank = [player, "LIEUTENANT"]; publicVariable "pVAR_changeRank";
	    pVAR_changeRank call wgp_fnc_setRank;
		private["_group"];
		_group = createGroup side player;
		[player] join _group;
	    pVAR_changeGroupID = [_group, gnewname]; publicVariable "pVAR_changeGroupID";
	    pVAR_changeGroupID call wgp_fnc_setGroupID;
		
		systemchat ("INFO: Squad " + gnewname + " created");
    };
};


/* Join */
if ((pixLogisticDialogHqGroupManager_Button == 2) && (pixLogisticDialogHqGroupManager_Selection != -1)) then{
	private["_group"];
	_group = _groups select pixLogisticDialogHqGroupManager_Selection;
    if (group player != _group) then {
		player setVariable ["CustomGroup",false,true];
	    pVAR_changeRank = [player, "CORPORAL"]; publicVariable "pVAR_changeRank";
	    pVAR_changeRank call wgp_fnc_setRank;
	
		[player] join _group;
		systemchat ("INFO: Joined """ + groupID _group + """") ;
    } else {
        systemchat ("INFO: You already are a member of """ + groupID _group + """");
    };
};
/* Leave */
if (pixLogisticDialogHqGroupManager_Button == 3) then {
	player setVariable ["CustomGroup",false,true];
    pVAR_changeRank = [player, "PRIVATE"]; publicVariable "pVAR_changeRank";
    pVAR_changeRank call wgp_fnc_setRank;
	systemchat ("INFO: You left """ + groupID (group player) + """");
	[player] join grpNull;
};

pixLogisticDialogHqGroupManager_Button = nil;
pixLogisticDialogHqGroupManager_Selection = nil;
