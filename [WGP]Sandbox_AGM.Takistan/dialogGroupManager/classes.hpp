#include "defines.hpp"

class PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER 
{
	idd = PIXLOGISTIC_IDD_DIALOG_HQ_GROUPMANAGER;
	name = "PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER";
	movingEnable = false;
	
	controlsBackground[] = 
	{
		PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_Title,
		PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_Background
	};
	objects[] = {};
	controls[] =
	{
		PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_SubTitle,
		PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_List,
		PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_EditName,		
		PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_ButtonCreate,
		PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_ButtonJoin,
		PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_ButtonLeave,
        
        PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_Members_List,
        PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_Members_SubTitle
	};
	
	class PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_Background: IGUIBack
	{
		idc = -1;
		x = GRID_ABS_X;
		y = GRID_ABS_Y;
		w = GRID_ABS_W;
		h = GRID_ABS_H + (2 * GRID_H);
	};	
	class PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_Title : RscTitle
	{
		idc = -1;
		x = GRID_ABS_X;
		y = GRID_ABS_Y - GRID_TITLE_H;
		w = GRID_ABS_W;
		h = GRID_TITLE_H;
		text = "Squad Manager";
	};	
	class PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_SubTitle : RscText
	{
		idc = PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_SubTitle;
		x = 1 * GRID_W + GRID_ABS_X; 
		y = 1 * GRID_H + GRID_ABS_Y; 
		w = 18 * GRID_W;
		h = 1 * GRID_H;
		text = "Squads";
		colorbackground[] = 
		{
			0,
			0,
			0,
			1
		};		
	};
	
	class PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_List : RscListBox
	{
		idc = PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_List;
		x = 1 * GRID_W + GRID_ABS_X; 
		y = 2 * GRID_H + GRID_ABS_Y; 
		w = 18 * GRID_W;
		h = 16 * GRID_H;
        onLBSelChanged = "execVM 'dialogGroupManager\onList_Clicked.sqf'";
	};
	

	class PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_ButtonCreate : RscButtonMenu
	{
		idc = PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_ButtonCreate;
		x = 31 * GRID_W + GRID_ABS_X; 
		y = 18.5 * GRID_H + GRID_ABS_Y; 
		w = 8 * GRID_W;
		h = 0.9 * GRID_H;
		text = "Create";
		font = "PuristaMedium";
		action = "execVM 'dialogGroupManager\onButtonCreateClicked.sqf';";
	};
	
	class PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_ButtonJoin : RscButtonMenu
	{
		idc = PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_ButtonJoin;
		x = 1 * GRID_W + GRID_ABS_X; 
		y = 20 * GRID_H + GRID_ABS_Y; 
		w = 18 * GRID_W;
		h = 0.9 * GRID_H;
		text = "Join";
		font = "PuristaMedium";
		action = "execVM 'dialogGroupManager\onButtonJoinClicked.sqf';";
	};
	class PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_ButtonLeave : RscButtonMenu
	{
		idc = PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_ButtonLeave;
		x = 1 * GRID_W + GRID_ABS_X; 
		y = 21 * GRID_H + GRID_ABS_Y; 
		w = 18 * GRID_W;
		h = 0.9 * GRID_H;
		text = "Leave";
		font = "PuristaMedium";
		action = "execVM 'dialogGroupManager\onButtonLeaveClicked.sqf';";
	};
	
	class PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_EditName : RscEdit
	{
		idc = PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_EditName;
		x = 1 * GRID_W + GRID_ABS_X; 
		y = 18.5 * GRID_H + GRID_ABS_Y; 
		w = 30 * GRID_W;
		h = 0.9 * GRID_H;
		text = "Group name";
		tooltip = "Define group name";
	};
    
    class PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_Members_SubTitle : RscText
	{
		idc = PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_Members_SubTitle;
		x = 20 * GRID_W + GRID_ABS_X; 
		y = 1 * GRID_H + GRID_ABS_Y; 
		w = 19 * GRID_W;
		h = 1 * GRID_H;
		text = "Members";
		colorbackground[] = 
		{
			0,
			0,
			0,
			1
		};		
	};
    
    class PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER_Members_List : RscListBox
	{
		idc = PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_Members_List;
		x = 20 * GRID_W + GRID_ABS_X; 
		y = 2 * GRID_H + GRID_ABS_Y; 
		w = 19 * GRID_W;
		h = 16 * GRID_H;
	};
};