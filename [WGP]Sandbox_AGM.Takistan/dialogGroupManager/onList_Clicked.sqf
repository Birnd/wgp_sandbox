#include "defines.hpp";
private["_groups", "_ctrl_members"];
_ctrl_members = PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_Members_List;
pixLogisticDialogHqGroupManager_Selection = lbCurSel PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_List;
lbclear _ctrl_members;
_groups = [];
{
	if (!(group _x in _groups) && side group _x == side player) then
	{
		private["_group","_CustomGroup","_gname"];
		_group = group _x;
		_CustomGroup = _group getVariable "CustomGroup";
		if (!isNil "_CustomGroup") then
		{
			_groups = _groups + [_group];
		};
		_gname = groupID _group;
	};
} foreach playableUnits;

_group = _groups select pixLogisticDialogHqGroupManager_Selection;
// player sidechat name _group;
{
    private["_squadinfo","_string"];
    _squadinfo = squadParams _x select 0;
    _string = name _x;
    if (!isNil "_squadinfo") then {
	    if (count _squadinfo > 0) then {
	    	if (_squadinfo select 0 != "") then { _string = _string + " " + (_squadinfo select 0)};
	    };
    };
	lbAdd [_ctrl_members, _string];
    if (!isNil "_squadinfo") then {
        if (count _squadinfo > 0) then {
            lbSetPicture[_ctrl_members, (lbSize _ctrl_members) -1, _squadinfo select 4];
            lbSetPictureColor[_ctrl_members, (lbSize _ctrl_members) -1, [1,1,1,1]];
        };
    }; 
} foreach units _group;