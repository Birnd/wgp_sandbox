#include "defines.hpp"
pixLogisticDialogBarrack_ButtonOK = 1; 
pixLogisticDialogBarrack_Selection = lbCurSel PIXLOGISTIC_IDC_DIALOG_BARRACK_List; 

private["_scriptFilename"];
_scriptFilename = (loadouts select pixLogisticDialogBarrack_Selection) select 1;
systemchat ("INFO: Kit equipped: " + ((loadouts select pixLogisticDialogBarrack_Selection) select 0));
this = player;
this setVariable ["AGM_GForceCoef", nil];
this setVariable ["AGM_IsEOD", nil];
this setVariable ["AGM_IsMedic", nil];

call compile preprocessFileLineNumbers ("barracks\loadouts\" + str (side player) + "\" + _scriptFilename + ".sqf");
this = Nil;
player setVariable ["playerKitID", pixLogisticDialogBarrack_Selection]; // player has to remember what kit he has. E.g. for kit customization.
closeDialog 0;
// _dummy = getPos player nearestObject "ReammoBox_F";
call compile preprocessFileLineNumbers ("barracks\kitcustomize.sqf");