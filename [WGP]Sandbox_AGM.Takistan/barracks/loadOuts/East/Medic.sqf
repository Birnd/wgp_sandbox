comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "rhs_uniform_mflora_patchless";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
for "_i" from 1 to 2 do {this addItemToUniform "rhs_mag_rdg2_white";};
this addItemToUniform "AGM_EarBuds";
this addVest "rhs_6b23_ML_medic";
this addItemToVest "rhs_mag_rgd5";
for "_i" from 1 to 8 do {this addItemToVest "rhs_30Rnd_545x39_7N10_AK";};
this addBackpack "rhs_sidor";
for "_i" from 1 to 20 do {this addItemToBackpack "AGM_Bandage";};
for "_i" from 1 to 8 do {this addItemToBackpack "AGM_Morphine";};
for "_i" from 1 to 8 do {this addItemToBackpack "AGM_Epipen";};
for "_i" from 1 to 4 do {this addItemToBackpack "AGM_Bloodbag";};
this addHeadgear "rhs_6b27m_green";

comment "Add weapons";
this addWeapon "rhs_weap_ak74m";
this addWeapon "Binocular";

this addItemToVest "rhs_30Rnd_545x39_7N10_AK";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "tf_fadak";
this linkItem "ItemWatch";