comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "rhs_uniform_cu_ocp_patchless";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
for "_i" from 1 to 3 do {this addItemToUniform "16Rnd_9x21_Mag";};
this addItemToUniform "AGM_EarBuds";
this addVest "rhsusf_iotv_ocp";
for "_i" from 1 to 6 do {this addItemToVest "rhsusf_5Rnd_300winmag_xm2010";};
this addHeadgear "rhs_Booniehat_ocp";

comment "Add weapons";
this addWeapon "rhs_weap_XM2010_d";
this addPrimaryWeaponItem "rhsusf_acc_anpeq15side";
this addPrimaryWeaponItem "rhsusf_acc_LEUPOLDMK4_2";
this addWeapon "hgun_P07_F";
this addWeapon "Binocular";

this addItemToVest "rhsusf_5Rnd_300winmag_xm2010";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "tf_anprc152";
this linkItem "ItemWatch";