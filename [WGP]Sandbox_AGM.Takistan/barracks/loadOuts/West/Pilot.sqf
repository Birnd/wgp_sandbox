comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "rhs_uniform_cu_ucp_patchless";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
this addItemToUniform "rhs_mag_30Rnd_556x45_M855A1_Stanag";
this addItemToUniform "AGM_EarBuds";
this addItemToUniform "SmokeShellRed";
this addVest "rhsusf_iotv_ucp";
for "_i" from 1 to 2 do {this addItemToVest "rhs_mag_30Rnd_556x45_M855A1_Stanag";};
this addHeadgear "rhsusf_hgu56p";

comment "Add weapons";
this addWeapon "rhs_weap_m4_carryhandle";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "tf_anprc152";
this linkItem "ItemGPS";
this linkItem "ItemWatch";