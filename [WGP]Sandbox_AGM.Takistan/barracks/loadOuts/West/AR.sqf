comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "rhs_uniform_cu_ocp_patchless";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addItemToUniform "AGM_EarBuds";
this addVest "rhsusf_iotv_ocp_SAW";
for "_i" from 1 to 2 do {this addItemToVest "rhsusf_100Rnd_556x45_soft_pouch";};
this addHeadgear "rhsusf_ach_helmet_ESS_ocp";

comment "Add weapons";
this addWeapon "rhs_weap_m249_pip";
this addWeapon "Binocular";

this addItemToVest "rhsusf_100Rnd_556x45_soft_pouch";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "tf_anprc152";
this linkItem "ItemWatch";