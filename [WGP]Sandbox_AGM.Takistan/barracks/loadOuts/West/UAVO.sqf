comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "rhs_uniform_cu_ocp_patchless";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addItemToUniform "rhs_mag_30Rnd_556x45_M855A1_Stanag";
this addItemToUniform "AGM_EarBuds";
this addVest "rhsusf_iotv_ocp_Rifleman";
this addItemToVest "HandGrenade";
for "_i" from 1 to 8 do {this addItemToVest "rhs_mag_30Rnd_556x45_M855A1_Stanag";};
this addBackpack "B_UAV_01_backpack_F";
this addHeadgear "rhsusf_ach_helmet_ocp";

comment "Add weapons";
this addWeapon "rhs_weap_m4_grip";
this addPrimaryWeaponItem "rhsusf_acc_anpeq15";
this addPrimaryWeaponItem "rhsusf_acc_compm4";
this addWeapon "Binocular";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "tf_anprc152";
this linkItem "B_UavTerminal";
this linkItem "ItemWatch";