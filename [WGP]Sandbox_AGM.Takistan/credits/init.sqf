﻿if (!isServer || !isDedicated) then
{
	waitUntil {!isNull player && player == player};	
	if(player diarySubjectExists "Credits") exitwith{};
	player createDiarySubject ["Credits","Credits"];
	player createDiaryRecord["Credits",["Credits","[WGP]Sandbox has been created by the Wargamer-Project.<br/>
					<br/>
                    The [TK]Persistent Campaign map has been used as a base, however almost everything has been reworked, expanded and improved by now.
					Even if - almost - all scripts are written by myself, the roots from most of the ideas are located in the community.<br/>
					<br/>
                    A big thank you to [TK]Pix for providing the initial mission. <br/>
					"]
	];
};