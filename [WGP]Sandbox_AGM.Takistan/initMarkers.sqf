waitUntil {!isNull player && isPlayer player};
sleep 3;
_icon3D_base = addMissionEventHandler ["Draw3D", {
    _distance = 100000;
    try {
    	_distance = (getPos player) distance (getMarkerPos "m_armory");
    } catch {
        _distance = 1000000;
    };
    if (_distance < 100) then {
        _alpha = 1;
        switch (true) do {
            case (_distance > 50): {_alpha = 1 - ((_distance - 50)*2/100)}; 
            default {_alpha = 1};
        };
        drawIcon3D [
	        "\A3\ui_f\data\map\vehicleicons\iconcrateammo_ca.paa",
	        [0.2,0.2,1,_alpha],
	        [
	            (getMarkerPos "m_armory") select 0,
	            (getMarkerPos "m_armory") select 1, 
	            6
	        ],
	        2 min ((2 / _distance) * 50),
	        2 min ((2 / _distance) * 50),
	        0,
	        "Armory",
	        1,
	        0.07 min ((0.07 / _distance)* 50), 
	        "PuristaMedium"
    	];
	};
    
    comment "REPAIR LAND Marker";
    try {
    	_distance = (getPos player) distance (getMarkerPos "m_armory");
    } catch {
        _distance = 1000000;
    };
    _distance = (getPos player) distance (getPos repland_base);
    if (_distance < 200 and (vehicle player iskindof "LandVehicle")) then {
        _alpha = 1;
        switch (true) do {
            case (_distance > 150): {_alpha = 1 - ((_distance - 150)*2/100)}; 
            default {_alpha = 1};
        };
        drawIcon3D [
	        "\a3\ui_f\data\map\Markers\NATO\respawn_armor_ca.paa",
	        [0.2,0.2,1,_alpha],
	        [
	            (getPos repland_base) select 0,
	            (getPos repland_base) select 1, 
	            6
	        ],
	        2 min ((2 / _distance) * 50),
	        2 min ((2 / _distance) * 50),
	        0,
	        "Maintenance",
	        1,
	        0.07 min ((0.07 / _distance)* 50), 
	        "PuristaMedium"
    	];
	};
    comment "REPAIR PLANE Marker";
    try {
    	_distance = (getPos player) distance (getPos repjet_base);
    } catch {
        _distance = 1000000;
    };
    if (_distance < 200 and (vehicle player iskindof "Plane")) then {
        _alpha = 1;
        switch (true) do {
            case (_distance > 150): {_alpha = 1 - ((_distance - 150)*2/100)}; 
            default {_alpha = 1};
        };
        drawIcon3D [
	        "\a3\ui_f\data\map\Markers\NATO\respawn_plane_ca.paa",
	        [0.2,0.2,1,_alpha],
	        [
	            (getPos repjet_base) select 0,
	            (getPos repjet_base) select 1, 
	            6
	        ],
	        2 min ((2 / _distance) * 50),
	        2 min ((2 / _distance) * 50),
	        0,
	        "Maintenance",
	        1,
	        0.07 min ((0.07 / _distance)* 50), 
	        "PuristaMedium"
    	];
	};
    comment "REPAIR HELI Marker";
    try {
   		 _distance = (getPos player) distance (getPos repheli_base);
    } catch {
        _distance = 1000000;
    };
    if (_distance < 200 and (vehicle player iskindof "Helicopter")) then {
        _alpha = 1;
        switch (true) do {
            case (_distance > 150): {_alpha = 1 - ((_distance - 150)*2/100)}; 
            default {_alpha = 1};
        };
        drawIcon3D [
	        "\a3\ui_f\data\map\Markers\NATO\respawn_air_ca.paa",
	        [0.2,0.2,1,_alpha],
	        [
	            (getPos repheli_base) select 0,
	            (getPos repheli_base) select 1, 
	            6
	        ],
	        2 min ((2 / _distance) * 50),
	        2 min ((2 / _distance) * 50),
	        0,
	        "Maintenance",
	        1,
	        0.07 min ((0.07 / _distance)* 50), 
	        "PuristaMedium"
    	];
	};
}];