/* Dialog erstellen*/
#include "defines.hpp";
createDialog "PIXLOGISTIC_DIALOG_HQ_GROUPMANAGER"; 	

/* Gruppen zusammensuchen */
private["_groups"];
_groups = [];
{
	if (!(group _x in _groups) && side group _x == playerside) then {
		private["_group","_leader","_hasGroup","_gname"];
		_group = group _x;
		_leader = leader _group;
		_hasGroup = _leader getVariable "HasGroup";
		if (!isNil "_hasGroup") then
		{
			if (_hasGroup) then
			{
				_groups = _groups + [_group];
			};
		};
		_gname = groupID _group;
	};
} foreach playableUnits;

/* Listbox füllen*/
{
	lbAdd [PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_List, groupID _x];
    _squadinfo = squadParams leader _x select 0;
    if (!isNil "_squadinfo") then {
    	if (count _squadinfo > 0) then { lbSetPicture[PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_List, (lbSize PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_List) -1, _squadinfo select 4] };
    };
} foreach _groups;
_squadinfo = squadParams player select 0;
if (!isNil "_squadinfo") then {
    if (count _squadinfo > 0) then { ctrlSetText [PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_EditName, _squadinfo select 0]};
};
if (count _groups > 0) then {
	lbSetCurSel [PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_List, 0];
};

/*-----------------------------------------------------------------	*/
/* Dialog anzeigen*/
pixLogisticDialogHqGroupManager_Button = -1;
pixLogisticDialogHqGroupManager_Selection = -1;
waitUntil { !dialog };
/*-----------------------------------------------------------------	*/

// Selected
if ((pixLogisticDialogHqGroupManager_Button == 0) && (pixLogisticDialogHqGroupManager_Selection != -1)) then 
{
    private "_ctrl_members";
	_ctrl_members = PIXLOGISTIC_IDC_DIALOG_HQ_GROUPMANAGER_Members_List;
    _group = _groups select pixLogisticDialogHqGroupManager_Selection;
    // player sidechat name _group;
    {
    	lbAdd [_ctrl_members, name _x];
    } foreach units _group;
};




/* Create */
if (pixLogisticDialogHqGroupManager_Button == 1) then 
{
	player setVariable ["HasGroup",true,true];
    pVAR_changeRank = [player, "COLONEL"]; publicVariable "pVAR_changeRank";
    pVAR_changeRank call wgp_fnc_setRank;
	private["_group"];
	_group = createGroup playerSide;
	[player] join _group;
    pVAR_changeGroupID = [_group, gnewname]; publicVariable "pVAR_changeGroupID";
    pVAR_changeGroupID call wgp_fnc_setGroupID;
	
	systemchat ("INFO: Squad " + gnewname + " created");
};


/* Join */
if ((pixLogisticDialogHqGroupManager_Button == 2) && (pixLogisticDialogHqGroupManager_Selection != -1)) then
{
	private["_group"];
	_group = _groups select pixLogisticDialogHqGroupManager_Selection;
    if (group player != _group) then { 
		player setVariable ["HasGroup",false,true];
	    pVAR_changeRank = [player, "CORPORAL"]; publicVariable "pVAR_changeRank";
	    pVAR_changeRank call wgp_fnc_setRank;
	
		[player] join _group;	
		systemchat ("INFO: Joined """ + groupID _group + """") ;
    } else {
        systemchat ("INFO: You already are a member of """ + groupID _group + """");
    };
};
/* Leave */
if (pixLogisticDialogHqGroupManager_Button == 3) then 
{
	player setVariable ["HasGroup",false,true];
    pVAR_changeRank = [player, "PRIVATE"]; publicVariable "pVAR_changeRank";
    pVAR_changeRank call wgp_fnc_setRank;
	systemchat ("INFO: You left """ + groupID (group player) + """");
	[player] join grpNull;
};

pixLogisticDialogHqGroupManager_Button = nil;
pixLogisticDialogHqGroupManager_Selection = nil;
