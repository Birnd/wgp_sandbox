if (!isServer || !isDedicated) then
{
    waitUntil { player == player };
	Sleep 2;
	
	private["_lastMarkerCount","_colors", "_sides"];
	_lastMarkerCount = 0;
    _colors = ["ColorBlue", "ColorRed", "ColorGreen", "ColorBlack"];
    _sides = [West, East, Resistance, Civilian];
	while {true} do {
		private["_units", "_color", "_side"];
		_units = [playableUnits, {isPlayer _x && (side _x ==  playerSide or side _x == civilian or player call wgp_fnc_isZeus)}] call BIS_fnc_conditionalSelect; // Only register player units on own side. Civs are added, because incapacitated players are civilians (=ignored by enemy AI).
		if (count _units != _lastMarkerCount) then {
			// Delete markers
			for "_i" from 0 to (_lastMarkerCount - 1) do {
				deleteMarkerLocal format["GpsMarker%1", _i];
			};
			
			//Recreate markers
			_lastMarkerCount = count _units;
			private ["_i", "_markerName"];
			for "_i" from 0 to (_lastMarkerCount - 1) do {
				private["_markerName"];
				_markerName = createMarkerLocal [format["GpsMarker%1", _i], [0,0,0]];
				_markerName setMarkerShapeLocal "ICON";
				_markerName setMarkerTypeLocal "b_inf";
				_markerName setMarkerSizeLocal [1, 1];
			};
		};
		
		// Refresh markers
		for "_i" from 0 to _lastMarkerCount-1 do {
			private["_unit"];
			_unit = _units select _i;
            if  (side _unit == playerSide or player call wgp_fnc_isZeus) then {
	            _rank = rank _unit;		
	
	            // Variables
				_markerName = format["GpsMarker%1", _i];
	
				_markerName setMarkerPosLocal (getPos _unit);
				if (captiveNum _unit == 1) then {
					_markerName setMarkerColorLocal "ColorBlack"; 
					_markerName setMarkerAlphaLocal 0.4;
					_markerName setMarkerTextLocal "*help*";
				} else {
                    if (_sides find (side _unit) != -1) then {
                    	_color = _colors select (_sides find side _unit);
                    } else {
                        _color = "ColorBlack";
                    };
                    
					_markerName setMarkerColorLocal _color;
					_markerName setMarkerAlphaLocal 0; //0.7, um alle Spieler sichtbar zu machen
					
					if (_rank == "COLONEL") then {
						_markerName setMarkerAlphaLocal 0.7; // Nur Spieler im Rank COLONEL (Std: Gruppenführer) anzeigen
						_markerName setMarkerTextLocal groupID (group _unit);
					} else {
                        if (player call wgp_fnc_isZeus) then {
                            _markerName setMarkerAlphaLocal 0.7;
                            _markerName setMarkerTextLocal name _unit;
                            _markerName setMarkerTypeLocal "mil_dot";
                            
                        };
                    };
					// if (captiveNum _unit == 3) then  { _markerName setMarkerAlphaLocal 0;}; // Nicht darstellen, wenn "captive" Status
					// _markerName setMarkerTextLocal (name _unit);
				};
			};
        };
		
		sleep 1;
	};
};