comment "Exported from Arsenal by [WGP]Senshi";

comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_C_Journalist";
this addItemToUniform "Laserbatteries";
this addVest "V_TacVest_blk";
for "_i" from 1 to 3 do {this addItemToVest "Laserbatteries";};
this addBackpack "tf_rt1523g_bwmod";
this addHeadgear "H_Cap_oli_hs";

comment "Add weapons";
this addWeapon "Laserdesignator";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "tf_anprc152_2";
this linkItem "ItemGPS";