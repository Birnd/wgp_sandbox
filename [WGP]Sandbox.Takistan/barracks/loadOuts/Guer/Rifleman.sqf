comment "Exported from Arsenal by [WGP]Senshi";

comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_B_CombatUniform_mcam";
for "_i" from 1 to 3 do {this addItemToUniform "30Rnd_65x39_caseless_mag";};
this addItemToUniform "30Rnd_65x39_caseless_mag_Tracer";
this addVest "V_TacVest_oli";
for "_i" from 1 to 3 do {this addItemToVest "x39_bandage";};
for "_i" from 1 to 4 do {this addItemToVest "HandGrenade";};
for "_i" from 1 to 2 do {this addItemToVest "SmokeShell";};
for "_i" from 1 to 4 do {this addItemToVest "30Rnd_65x39_caseless_mag";};
this addHeadgear "H_HelmetB_light";
this addGoggles "G_Tactical_Clear";

comment "Add weapons";
this addWeapon "arifle_MX_F";
this addMagazine "30Rnd_65x39_caseless_mag";
this addPrimaryWeaponItem "optic_Arco";
this addWeapon "Binocular";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "tf_anprc152_3";
this linkItem "NVGoggles";