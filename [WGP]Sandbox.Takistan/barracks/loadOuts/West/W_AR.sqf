comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "rhs_uniform_cu_ocp";
this addItemToUniform "FirstAidKit";
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addItemToUniform "rhsusf_100Rnd_556x45_soft_pouch";
this addVest "rhsusf_iotv_ocp_SAW";
for "_i" from 1 to 5 do {this addItemToVest "rhsusf_100Rnd_556x45_soft_pouch";};
this addHeadgear "rhsusf_ach_helmet_ESS_ocp";

comment "Add weapons";
this addWeapon "rhs_weap_m249_pip";
this addPrimaryWeaponItem "rhsusf_acc_ELCAN";
this addWeapon "Binocular";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "tf_microdagr";
this linkItem "ItemRadio";