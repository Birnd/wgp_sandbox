comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "rhs_uniform_cu_ocp";
this addItemToUniform "FirstAidKit";
this addItemToUniform "rhs_mag_30Rnd_556x45_Mk262_Stanag";
this addItemToUniform "SmokeShellRed";
this addItemToUniform "SmokeShellBlue";
this addVest "rhsusf_iotv_ocp_Squadleader";
for "_i" from 1 to 6 do {this addItemToVest "rhs_mag_30Rnd_556x45_Mk262_Stanag";};
this addItemToVest "HandGrenade";
this addHeadgear "rhsusf_ach_helmet_camo_ocp";

comment "Add weapons";
this addWeapon "rhs_weap_m16a4_grip";
this addPrimaryWeaponItem "rhsusf_acc_anpeq15";
this addPrimaryWeaponItem "rhsusf_acc_ACOG2";
this addWeapon "LaserDesignator";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "tf_microdagr";
this linkItem "ItemRadio";
this linkItem "ItemGPS";