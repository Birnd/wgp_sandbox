comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "rhs_uniform_cu_ocp";
this addItemToUniform "FirstAidKit";
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addItemToUniform "rhs_mag_30Rnd_556x45_M855A1_Stanag";
this addItemToUniform "rhs_mag_M433_HEDP";
this addVest "rhsusf_iotv_ocp_Teamleader";
for "_i" from 1 to 6 do {this addItemToVest "rhs_mag_M433_HEDP";};
for "_i" from 1 to 4 do {this addItemToVest "rhs_mag_30Rnd_556x45_M855A1_Stanag";};
for "_i" from 1 to 2 do {this addItemToVest "rhs_mag_m714_White";};
for "_i" from 1 to 2 do {this addItemToVest "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red";};
this addHeadgear "rhsusf_ach_helmet_headset_ess_ocp";

comment "Add weapons";
this addWeapon "rhs_m4a1_m320";
this addPrimaryWeaponItem "rhsusf_acc_anpeq15";
this addPrimaryWeaponItem "rhsusf_acc_ACOG";
this addWeapon "Rangefinder";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "tf_microdagr";
this linkItem "ItemRadio";
this linkItem "ItemGPS";