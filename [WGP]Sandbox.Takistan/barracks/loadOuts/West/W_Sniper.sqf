comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_B_GhillieSuit";
this addItemToUniform "FirstAidKit";
this addItemToUniform "7Rnd_408_Mag";
this addItemToUniform "16Rnd_9x21_Mag";
this addItemToUniform "SmokeShell";
this addVest "rhsusf_iotv_ocp";
for "_i" from 1 to 5 do {this addItemToVest "7Rnd_408_Mag";};
for "_i" from 1 to 2 do {this addItemToVest "16Rnd_9x21_Mag";};

comment "Add weapons";
this addWeapon "srifle_LRR_camo_F";
this addPrimaryWeaponItem "optic_LRPS";
this addWeapon "hgun_P07_F";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "tf_microdagr";
this linkItem "ItemRadio";