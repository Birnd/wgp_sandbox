comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "rhs_uniform_cu_ocp";
this addItemToUniform "FirstAidKit";
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addItemToUniform "rhs_mag_30Rnd_556x45_M855A1_Stanag";
this addVest "rhsusf_iotv_ocp_Rifleman";

this addItemToVest "rhs_m136_mag";
this addWeapon "rhs_weap_M136";

this addItemToVest "HandGrenade";
for "_i" from 1 to 8 do {this addItemToVest "rhs_mag_30Rnd_556x45_M855A1_Stanag";};
this addHeadgear "rhsusf_ach_helmet_ESS_ocp";

comment "Add weapons";
this addWeapon "rhs_weap_m4a1_grip";
this addPrimaryWeaponItem "rhsusf_acc_anpeq15";
this addPrimaryWeaponItem "rhsusf_acc_ACOG";
this addWeapon "Binocular";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "tf_microdagr";
this linkItem "ItemRadio";