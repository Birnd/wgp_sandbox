private["_scriptFilename", "_result", "_loadouts"];
disableSerialization;
#include "defines.hpp"
pixLogisticDialogBarrack_Selection = lbCurSel PIXLOGISTIC_IDC_DIALOG_BARRACK_List;
_ctrl_weapons = PIXLOGISTIC_IDC_DIALOG_BARRACK_Weapons_List;
lbClear _ctrl_weapons;

KK_fnc_inString = {
    /*
    Author: Killzone_Kid
    
    Description:
    Find a string within a string (case insensitive)
    
    Parameter(s):
    _this select 0: <string> string to be found
    _this select 1: <string> string to search in
    
    Returns:
    Boolean (true when string is found)
    
    How to use:
    _found = ["needle", "Needle in Haystack"] call KK_fnc_inString;
    */

    private ["_needle","_haystack","_needleLen","_hay","_found"];
    _needle = [_this, 0, "", [""]] call BIS_fnc_param;
    _haystack = toArray ([_this, 1, "", [""]] call BIS_fnc_param);
    _needleLen = count toArray _needle;
    _hay = +_haystack;
    _hay resize _needleLen;
    _found = false;
    for "_i" from _needleLen to count _haystack do {
        if (toString _hay == _needle) exitWith {_found = true};
        _hay set [_needleLen, _haystack select _i];
        _hay set [0, "x"];
        _hay = _hay - ["x"]
    };
    _found
};

KK_fnc_returnUnique = {
	private "_arr";
	_arr = [];
	while {!(_this isEqualTo [])} do {
		_arr pushBack (_this select 0);
		_this = _this - _arr;
	};
	_arr
};

WGP_fnc_findClass = {
    if isText(configFile >> "CfgWeapons" >> _this >> "displayName") then {
        configFile >> "CfgWeapons" >> _this
    } else {
        if isText(configFile >> "CfgMagazines" >> _this >> "displayName") then {
	        configFile >> "CfgMagazines" >> _this
	    } else {
	        if isText(configFile >> "CfgVehicles" >> _this >> "displayName") then {
		        configFile >> "CfgVehicles" >> _this
	        } else {
		        if isText(configFile >> "CfgGlasses" >> _this >> "displayName") then {
			        configFile >> "CfgGlasses" >> _this
                }
            }
        }
    };
};

_getDisplayName = {getText((_this call WGP_fnc_findClass) >> "displayName")};
_getPic =  {getText((_this call WGP_fnc_findClass) >> "picture")};


// Item counting

_scriptFilename = (loadouts select pixLogisticDialogBarrack_Selection) select 1;
call compile preprocessFileLineNumbers ("barracks\loadouts\" + str playerside + "\" + _scriptFilename + ".sqf");


_hmd = hmd this; // Head-Mounted Display = NVG
_headgear = headgear this;
_binocular = binocular this;
_uniform = uniform this;
_vest = vest this;
_backpack = backpack this;
_goggles = goggles this;

_primaryWeapon = primaryWeapon this;
// _primaryWeaponMagazine = primaryWeaponMagazine this;
_secondaryWeapon = secondaryWeapon this;
// _scondaryWeaponMagazine = secondaryWeaponMagazine this;
_handgunWeapon = handgunWeapon this;
// _handgunMagazine = handgunMagazine this;

_weapons = weapons this;
_magazines = magazines this;
_items = items this;
_assigneditems = assignedItems this;
_vestitems = vestitems this;
_uniformitems = uniformitems this;
_backpackitems = backpackitems this;
_handgunWeaponItems = handgunitems this;
_primaryWeaponItems = primaryWeaponItems this;
_secondaryWeaponItems = secondaryWeaponItems this;

_items_flat = _magazines + _items + _assigneditems;
_items_unique = _items_flat call KK_fnc_returnUnique;

if (_primaryWeapon != "") then {
	lbAdd [_ctrl_weapons, _primaryWeapon call _getDisplayName];
	lbSetPicture[_ctrl_weapons, (lbSize _ctrl_weapons) -1, _primaryWeapon call _getPic];
	{
	    if (_x != "") then {
		    lbAdd [_ctrl_weapons, _x call _getDisplayName];
			lbSetPicture[_ctrl_weapons, (lbSize _ctrl_weapons) -1, _x call _getPic];
	    };
	} foreach _primaryWeaponItems;
};
if (_secondaryWeapon != "") then {
	lbAdd [_ctrl_weapons, _secondaryWeapon call _getDisplayName];
	lbSetPicture[_ctrl_weapons, (lbSize _ctrl_weapons) -1, _secondaryWeapon call _getPic];
	{
	    if (_x != "") then {
		    lbAdd [_ctrl_weapons, _x call _getDisplayName];
			lbSetPicture[_ctrl_weapons, (lbSize _ctrl_weapons) -1, _x call _getPic];
	    };
	} foreach _secondaryWeaponItems;
};
if (_handgunWeapon != "") then {
	lbAdd [_ctrl_weapons, _handgunWeapon call _getDisplayName];
	lbSetPicture[_ctrl_weapons, (lbSize _ctrl_weapons) -1, _handgunWeapon call _getPic];
	{
	    if (_x != "") then {
		    lbAdd [_ctrl_weapons, _x call _getDisplayName];
			lbSetPicture[_ctrl_weapons, (lbSize _ctrl_weapons) -1, _x call _getPic];
	    };
	} foreach _handgunWeaponItems;
};
{
    _item = _x;
    _count = count([_items_flat, {_x == _item}] call BIS_fnc_conditionalSelect);
    if (_item != "") then {
	    if (_count > 1) then {
		    lbAdd [_ctrl_weapons, (format ["%1x %2 ",_count, _item call _getDisplayName])];
		    lbSetPicture[_ctrl_weapons, (lbSize _ctrl_weapons) -1, _item call _getPic];
	    } else {
	        lbAdd [_ctrl_weapons, _item call _getDisplayName];
	        lbSetPicture[_ctrl_weapons, (lbSize _ctrl_weapons) -1, _item call _getPic];
	    };
    };
} foreach _items_unique;

if (_goggles != "") then {
	lbAdd [_ctrl_weapons, _goggles call  _getDisplayName];
    lbSetPicture[_ctrl_weapons, (lbSize _ctrl_weapons) -1, _goggles call _getPic];
};
if (_headgear != "") then {
	lbAdd [_ctrl_weapons, _headgear call  _getDisplayName];
    lbSetPicture[_ctrl_weapons, (lbSize _ctrl_weapons) -1, _headgear call _getPic];
};
if (_uniform != "") then {
	lbAdd [_ctrl_weapons, _uniform call  _getDisplayName];
    lbSetPicture[_ctrl_weapons, (lbSize _ctrl_weapons) -1, _uniform call _getPic];
};
if (_vest != "") then {
	lbAdd [_ctrl_weapons, _vest call  _getDisplayName];
    lbSetPicture[_ctrl_weapons, (lbSize _ctrl_weapons) -1, _vest call _getPic];
};
if (_backpack != "") then {
	lbAdd [_ctrl_weapons, _backpack call  _getDisplayName];
    lbSetPicture[_ctrl_weapons, (lbSize _ctrl_weapons) -1, _backpack call _getPic];
};