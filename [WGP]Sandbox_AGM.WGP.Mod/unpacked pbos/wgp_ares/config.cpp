class CfgPatches {
	class Ares {
		units[] = {
			"Ares_Module_Add_All_Objects_To_Curator",
			"Ares_Module_Arsenal_AddCustom",
			"Ares_Module_Arsenal_Copy_To_Clipboard",
			"Ares_Module_Arsenal_Create_Aaf",
			"Ares_Module_Arsenal_Create_Csat",
			"Ares_Module_Arsenal_Create_Guerilla",
			"Ares_Module_Arsenal_Create_Nato",
			"Ares_Module_Arsenal_Paste_Combine",
			"Ares_Module_Arsenal_Paste_Replace",
			"Ares_Module_Behaviour_Create_Artillery_Target",
			"Ares_Module_Behaviour_Fire_At_Artillery_Target",
			"Ares_Module_Behaviour_Patrol",
			"Ares_Module_Behaviour_Search_Nearby_And_Garrison",
			"Ares_Module_Behaviour_Search_Nearby_Building",
			"Ares_Module_Equipment_Lights",
			"Ares_Module_Equipment_Nvgs",
			"Ares_Module_Equipment_Thermals",
			"Ares_Module_Garrison_Nearest",
			"Ares_Module_Paste_Objects_Absolute",
			"Ares_Module_Paste_Objects_Relative",
			"Ares_Module_Reinforcements_Create_Lz",
			"Ares_Module_Reinforcements_Create_Rp",
			"Ares_Module_Reinforcements_Spawn_Units",
			"Ares_Module_Remove_Objects_From_Curator",
			"Ares_Module_Save_Objects_For_Paste",
			"Ares_Module_Spawn_Custom_Mission_Objects",
			"Ares_Module_Spawn_Smoke",
			"Ares_Module_Spawn_Submarine",
			"Ares_Module_Spawn_Trawler",
			"Ares_Module_SurrenderSingleUnit",
			"Ares_Module_Teleport_All",
			"Ares_Module_Teleport_CreateTeleporter",
			"Ares_Module_Teleport_Group",
			"Ares_Module_Teleport_Side_Civilian",
			"Ares_Module_Teleport_Side_Csat",
			"Ares_Module_Teleport_Side_Independent",
			"Ares_Module_Teleport_Side_Nato",
			"Ares_Module_Teleport_Single",
			"Ares_Module_Teleport_Zeus",
			"Ares_Module_UnGarrison",
			"Ares_Module_Util_Create_Composition",
			"Ares_Module_Util_Remove_Nearest_Persistent",
			"Ares_Module_Util_Change_Weather",
			"Ares_Module_Behaviours_Limit_AI",
			"Ares_module_Behaviours_Set_Combat_Mode",
			"Ares_Module_Util_ChangeSide",
			"Ares_Module_Util_setHit",
			"Ares_Module_Util_RemoveGear"
		};
	};
  class wgpares {
    units[] = {};
    weapons[] = {};
    requiredAddons[] = {
    		"A3_UI_F",
			"A3_UI_F_Curator",
			"A3_Functions_F",
			"A3_Functions_F_Curator",
			"A3_Modules_F",
			"A3_Modules_F_Curator",
			"Ares"};
  };
};

class CfgVehicles
{	
	class Ares_Behaviours_Module_Base;
	class Ares_Util_Module_Base;
	class Ares_Module_Behaviours_Limit_AI : Ares_Behaviours_Module_Base
	{
		scopeCurator = 2;
		displayName = "Limit AI";
		function = "WGP_Ares_fnc_BehaviourLimitAI";
	};
	class Ares_Module_Behaviours_Set_Combat_Mode : Ares_Behaviours_Module_Base
	{
		scopeCurator = 2;
		displayName = "Set Combat Mode";
		function = "WGP_Ares_fnc_BehaviourSetCombatMode";
 	};
 	
 	
	
	class Ares_Module_Util_ChangeSide : Ares_Util_Module_Base
	{
		scopeCurator = 2;
		displayName = "Change Side";
		function = "WGP_Ares_fnc_changeside";
	};
	
	class Ares_Module_Util_SetHit : Ares_Util_Module_Base
	{
		scopeCurator = 2;
		displayName = "Set Hit";
		function = "WGP_Ares_fnc_SetHit";
	};
	
	class Ares_Module_Util_RemoveGear : Ares_Util_Module_Base
	{
		scopeCurator = 2;
		displayName = "Remove Gear";
		function = "WGP_Ares_fnc_RemoveGear";
	};
	
};


class CfgFunctions
{
	class WGP_Ares
	{
		class BehaviourModules
		{
			file = "\wgp_ares\modules\Behaviour";
			class BehaviourLimitAi;
			class BehaviourSetCombatMode;
		};
		class UtilModules
		{
			file = "\wgp_ares\modules\Util";
			class ChangeSide;
			class SetHit;
			class RemoveGear;
		};

	};
};
