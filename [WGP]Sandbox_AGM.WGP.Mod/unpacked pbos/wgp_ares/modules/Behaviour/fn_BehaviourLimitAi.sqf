private ["_logic", "_units", "_activated"];
#include "\ares_zeusExtensions\module_header.hpp"

_unitUnderCursor = [_logic, false] call Ares_fnc_GetUnitUnderCursor;

if (isNull _unitUnderCursor) then {
	[objnull, format ["ERROR: Must be placed on unit"]] call bis_fnc_showCuratorFeedbackMessage;
}else {
	_dialogResult = [
		"Limit Ai Component",
		[
			["Change only:", ["Unit", "Group"], 1],
			["Part:", ["Move", "Autotarget", "Target", "Anim","Fsm"]],
			["Enable/Disable:",["Disable","Enable"]]
		]
	] call Ares_fnc_ShowChooseDialog;
	
	if (count _dialogResult > 0) then {
		_dialogUnitsToAffect = _dialogResult select 0;
		_dialogPart = _dialogResult select 1;
		_dialogEnable = _dialogResult select 2;

		private ["_units", "_part"];
		_units = [];
		switch (_dialogUnitsToAffect) do {
			case 0: {_units = crew vehicle _unitUnderCursor};
			case 1: {_units = units group _unitUnderCursor};
		};

		switch (_dialogPart) do {
			case 0: {_part = "MOVE"};
			case 1: {_part = "AUTOTARGET"};
			case 2: {_part = "TARGET"};
			case 3: {_part = "ANIM"};
			case 4: {_part = "FSM"};
		};
		
		switch (_dialogEnable) do {
			case 0: {{_x disableAI _part;} foreach _units;
					[objnull, format ["%1 units disabled %2", count _units, _part]] call bis_fnc_showCuratorFeedbackMessage;};
			case 1: {{_x enableAI _part;} foreach _units;
					[objnull, format ["%1 units enabled %2", count _units, _part]] call bis_fnc_showCuratorFeedbackMessage;};
		};
	};
};

#include "\ares_zeusExtensions\module_footer.hpp"