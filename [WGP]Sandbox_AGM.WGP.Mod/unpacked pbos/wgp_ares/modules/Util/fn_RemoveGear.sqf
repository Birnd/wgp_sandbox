private ["_logic", "_units", "_activated"];
#include "\ares_zeusExtensions\module_header.hpp"

_unitUnderCursor = [_logic, false] call Ares_fnc_GetUnitUnderCursor;


if (isNull _unitUnderCursor) then {
	[objnull, format ["ERROR: Must be placed on unit"]] call bis_fnc_showCuratorFeedbackMessage;
} else {
	_dialogResult = [
		"Remove Gear",
		[
			["From:", ["Unit", "Group"], 1]
		]
	] call Ares_fnc_ShowChooseDialog;
	
	if (count _dialogResult > 0) then {
		_dialogUnitsToAffect = _dialogResult select 0;

		private ["_units"];
		_units = [];
		switch (_dialogUnitsToAffect) do {
			case 0: {_units = crew vehicle _unitUnderCursor};
			case 1: {_units = units group _unitUnderCursor};
		};

		{removeAllWeapons _x;
		removeAllItems _x;
		removeAllAssignedItems _x;
		removeHeadgear _x;
		}foreach _units;
		
		[objnull, format ["Removed Gear from %1 units", count _units]] call bis_fnc_showCuratorFeedbackMessage;
	};
};

#include "\ares_zeusExtensions\module_footer.hpp"