private ["_logic", "_units", "_activated"];
#include "\ares_zeusExtensions\module_header.hpp"

_unitUnderCursor = [_logic, false] call Ares_fnc_GetUnitUnderCursor;


if (isNull _unitUnderCursor) then {
	[objnull, format ["ERROR: Must be placed on unit"]] call bis_fnc_showCuratorFeedbackMessage;
} else {
	_dialogResult = [
		"Change side to",
		[
			["Change only:", ["Unit", "Group"], 1],
			["to side:", ["West", "East", "Resistance", "Civilian"]]
		]
	] call Ares_fnc_ShowChooseDialog;
	
	if (count _dialogResult > 0) then {
		_dialogUnitsToAffect = _dialogResult select 0;
		_dialogNewSide = _dialogResult select 1;

		private ["_units", "_desiredSide"];
		_units = [];
		switch (_dialogUnitsToAffect) do {
			case 0: {_units = crew vehicle _unitUnderCursor};
			case 1: {_units = units group _unitUnderCursor};
		};

		switch (_dialogNewSide) do {
			case 0: {_desiredSide = West};
			case 1: {_desiredSide = East};
			case 2: {_desiredSide = Independent};
			case 3: {_desiredSide = Civilian};
		};

		_newGrp = createGroup _desiredSide;
		_units join _newGrp;
		[objnull, format ["%1 units changed to %2", count _units, _desiredSide]] call bis_fnc_showCuratorFeedbackMessage;
	};
};

#include "\ares_zeusExtensions\module_footer.hpp"